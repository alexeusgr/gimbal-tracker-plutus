{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Escrow.EscrowCompiler where

import Cardano.Api
import Cardano.Api.Shelley (PlutusScript (..))
import Codec.Serialise (serialise)
import Data.Aeson
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Short as SBS
import qualified Ledger

import Plutus.V1.Ledger.Api (Data (B, Constr, I, List, Map), ToData, toData)

import Escrow.BountyEscrow
import Escrow.BountyTreasury

dataToScriptData :: Data -> ScriptData
dataToScriptData (Constr n xs) = ScriptDataConstructor n $ dataToScriptData <$> xs
dataToScriptData (I n) = ScriptDataNumber n
dataToScriptData (B b) = ScriptDataBytes b
dataToScriptData (Map xs) = ScriptDataMap [(dataToScriptData k, dataToScriptData v) | (k, v) <- xs]
dataToScriptData (List xs) = ScriptDataList $ fmap dataToScriptData xs

writeJson :: ToData a => FilePath -> a -> IO ()
writeJson file = LBS.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . dataToScriptData . toData

writeValidator :: FilePath -> Ledger.Validator -> IO (Either (FileError ()) ())
writeValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV1) file Nothing . PlutusScriptSerialised . SBS.toShort . LBS.toStrict . serialise . Ledger.unValidatorScript

-- writeMyFirstValidatorScript :: IO (Either (FileError ()) ())
-- writeMyFirstValidatorScript = writeValidator "output/bounty-v0.plutus" Escrow.BountyEscrowDraft.validator

writeBountyEscrowScript :: IO (Either (FileError ()) ())
writeBountyEscrowScript = writeValidator "mainnet-v2/output/mainnet-gimbal-bounty-escrow-v2.plutus" $ Escrow.BountyEscrow.validator $ BountyParam
    {
      bountyTokenPolicyId = "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30"
    , bountyTokenName     = "gimbal"
    , accessTokenPolicyId = "68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67"
    , treasuryIssuerPkh   = "226bd2f54497a168b20306b7d7ef429a755eb0d612f3339e5aa622d2"
    }

writeBountyTreasuryScript :: IO (Either (FileError ()) ())
writeBountyTreasuryScript = writeValidator "mainnet-v2/output/mainnet-gimbal-bounty-treasury-v2.plutus" $ Escrow.BountyTreasury.validator $ TreasuryParam
    {
      atSymbol            = "68ae22138b3c82c717713d850e5ee57c7de5de8591f5f13cd3a6cc67"
    , bountyContract      = "2a74fb8c10d9eea0025046dc5d116b23687f209d513662d783762909"
    , bSymbol             = "2b0a04a7b60132b1805b296c7fcb3b217ff14413991bf76f72663c30"
    , bName               = "gimbal"
    , tIssuerPkh          = "226bd2f54497a168b20306b7d7ef429a755eb0d612f3339e5aa622d2"
    }