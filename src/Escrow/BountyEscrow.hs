{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}

module Escrow.BountyEscrow where

import              Ledger              hiding (singleton)
import              Ledger.Typed.Scripts
import              Ledger.Value        as Value
import              Ledger.Ada
import qualified    PlutusTx
import              PlutusTx.Prelude    hiding (Semigroup (..), unless)
import              Prelude             (Show (..))

-- V2: Add Issuer's PKH as treasury Param
-- The treasuryIssuerPkh is included here in order to create a unique Bounty Escrow contract and address
data BountyParam = BountyParam
    { bountyTokenPolicyId     :: !CurrencySymbol
    , bountyTokenName         :: !TokenName
    , accessTokenPolicyId     :: !CurrencySymbol
    , treasuryIssuerPkh       :: !PubKeyHash
    }

PlutusTx.makeLift ''BountyParam

data BountyEscrowDatum = BountyEscrowDatum
  {
    issuerPkh           :: !PubKeyHash
  , contributorPkh  :: !PubKeyHash
  , lovelaceAmount      :: !Integer
  , tokenAmount         :: !Integer
  , expirationTime      :: !POSIXTime
  }

PlutusTx.unstableMakeIsData ''BountyEscrowDatum

-- Usage: a bounty is created when a Contributor commits to it
-- Before that it is ready + "listed" off-chain (and here's a cool chance to create a Haskell API server)
-- v0 implements a "good faith" escrow, where funds are held, but still controlled by the bounty issuer
-- an upcoming iteration can use a token to represent each bounty, and that token can be used for unlocking
-- (which raises the question, who holds that token, and when?)
-- In any case, for now, the following actions will do:

data BountyAction = Cancel | Update | Distribute
  deriving Show

PlutusTx.makeIsDataIndexed ''BountyAction [('Cancel, 0), ('Update, 1), ('Distribute, 2)]
PlutusTx.makeLift ''BountyAction


{-# INLINEABLE mkValidator #-}
mkValidator :: BountyParam -> BountyEscrowDatum -> BountyAction -> ScriptContext -> Bool
mkValidator bp dat action ctx =
  case action of
    Cancel      ->  traceIfFalse "Only Issuer can Cancel Bounty"                signedByIssuer &&
                    traceIfFalse "Can only cancel bounty after deadline"        deadlineReached
    Update      ->  traceIfFalse "Only Issuer can Update Bounty"                signedByIssuer &&
                    traceIfFalse "Update must create one new Bounty UTXO"       createsContinuingBounty &&
                    traceIfFalse "Output UTXO value must be geq datum specs"    outputFulfillsValue
                    -- check that datum is updated accurately
                    -- traceIfFalse "New expiration time must be on or after old"  extendsExpirationTime &&
                    -- traceIfFalse "Bounty amounts can only be increased"         increasesBounty
    Distribute  ->  traceIfFalse "Issuer must sign to distribute bounty"        signedByIssuer &&
                    traceIfFalse "Contributor must receive full bounty values"  outputFulfillsBounty

  where
    info :: TxInfo
    info = scriptContextTxInfo ctx

    signedByIssuer :: Bool
    signedByIssuer = txSignedBy info $ issuerPkh dat

    deadlineReached :: Bool
    deadlineReached = contains (from $ expirationTime dat) $ txInfoValidRange info

    -- Update means that a UTXO must be left at contract address
    outputsToContract :: [TxOut]
    outputsToContract = getContinuingOutputs ctx

    createsContinuingBounty :: Bool
    createsContinuingBounty = length outputsToContract == 1

    outputContainsValue :: [TxOut] -> Bool
    outputContainsValue [x]   = (valueOf (txOutValue x) (bountyTokenPolicyId bp) (bountyTokenName bp)) >= (tokenAmount dat) &&
                                (getLovelace $ fromValue $ txOutValue x) >= (lovelaceAmount dat)
    outputContainsValue _     = False

    outputFulfillsValue :: Bool
    outputFulfillsValue = outputContainsValue outputsToContract

    valueToContributor :: Value
    valueToContributor = valuePaidTo info $ contributorPkh dat

    -- The value sent to Contributor must be at least the amount specified by bounty
    -- contributor must get tokenAmount bp of gimbals and lovelaceAmount bp...
    outputFulfillsBounty :: Bool
    outputFulfillsBounty = (valueOf valueToContributor (bountyTokenPolicyId bp) (bountyTokenName bp)) >= (tokenAmount dat) &&
                           (getLovelace $ fromValue valueToContributor) >= (lovelaceAmount dat)

    -- START TASK --------------------------------------------------------------------------------------------------------------
    -- PPBL Bounty Task:
    -- write extendsExpirationTime and increasesBounty that compare datum on incoming UTXO to datum on outgoing UTXO

    -- returns true if updated expirationTime is on or after previous expirationTime
    -- extendsExpirationTime :: Bool
    -- extendsExpirationTime = True

    -- returns true if updated lovelaceAmount and tokenAmount are greater than or equal to previous
    -- increasesBounty :: Bool
    -- increasesBounty = True

    -- Use findDatum or findDatumHash?
    -- Docs: https://playground.plutus.iohkdev.io/doc/haddock/plutus-ledger-api/html/Plutus-V1-Ledger-Contexts.html#v:findDatum

    -- END TASK ----------------------------------------------------------------------------------------------------------------

data EscrowTypes

instance ValidatorTypes EscrowTypes where
    type DatumType EscrowTypes = BountyEscrowDatum
    type RedeemerType EscrowTypes = BountyAction

typedValidator :: BountyParam -> TypedValidator EscrowTypes
typedValidator bp =
  mkTypedValidator @EscrowTypes
    ($$(PlutusTx.compile [||mkValidator||]) `PlutusTx.applyCode` PlutusTx.liftCode bp)
    $$(PlutusTx.compile [||wrap||])
  where
    wrap = wrapValidator @BountyEscrowDatum @BountyAction

validator :: BountyParam -> Validator
validator = validatorScript . typedValidator
